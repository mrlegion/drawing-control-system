﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DCS.Logic.Common;
using DCS.Logic.Interface.Components;
using NUnit.Framework;

namespace DCS.Logic.Storagekeeper.Tests
{
    [TestFixture]
    public class DCSStoragekeeperTests
    {
        private IStoragekeeper storagekeeper = null;
        private Drawing drawing = null;

        [SetUp]
        public void Init()
        {
            drawing = new Drawing()
            {
                Width = 297,
                Height = 210,
                Area = 297 * 210,
                Format = "a4",
                Include = 1,
                Orientation = OrientationType.Horizontal,
                IsMarker = false
            };

            storagekeeper = new Components.Storagekeeper();
        }

        [Test]
        public void DCS_SK_Add()
        {
            storagekeeper.Add(drawing: drawing);
            Assert.AreEqual(1, storagekeeper.Drawings.Count);
        }

        [Test]
        public void DCS_SK_Clear()
        {
            storagekeeper.Add(drawing: drawing);
            storagekeeper.Clear();
            Assert.AreEqual(0, storagekeeper.Drawings.Count);
        }
    }
}
