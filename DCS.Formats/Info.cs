﻿using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using DCS.Helpers.Common;

namespace DCS.Helpers
{
    public class Info
    {
        private List<Format> formats = null;
        private List<Marker> markers = null;

        public Info()
        {
            formats = new List<Format>()
            {
                new Format(name: "a5",         width: 210,  height: 148, area: 31080,  lowArea: 27600,  upArea: 34760,   includes: 0.5),
                new Format(name: "a4",         width: 297,  height: 210, area: 62370,  lowArea: 57400,  upArea: 67540,   includes: 1),
                new Format(name: "a3",         width: 420,  height: 297, area: 124740, lowArea: 117670, upArea: 132010,  includes: 2),
                new Format(name: "a2",         width: 420,  height: 297, area: 124740, lowArea: 117670, upArea: 132010,  includes: 2),
                new Format(name: "a1",         width: 841,  height: 594, area: 499554, lowArea: 485304, upArea: 514004,  includes: 8),
                new Format(name: "a0",         width: 1188, height: 841, area: 999108, lowArea: 978918, upArea: 1019498, includes: 16),
                new Format(name: "297 x 630",  width: 630,  height: 297, area: 187110, lowArea: 177940, upArea: 196480,  includes: 3),
                new Format(name: "297 x 840",  width: 840,  height: 297, area: 249480, lowArea: 238210, upArea: 260950,  includes: 4),
                new Format(name: "297 x 1050", width: 1050, height: 297, area: 311850, lowArea: 298480, upArea: 325420,  includes: 5),
                new Format(name: "297 x 1260", width: 1260, height: 297, area: 374220, lowArea: 358750, upArea: 389890,  includes: 6),
                new Format(name: "297 x 1470", width: 1470, height: 297, area: 436590, lowArea: 419020, upArea: 454360,  includes: 7),
                new Format(name: "594 x 630",  width: 630,  height: 594, area: 374220, lowArea: 362080, upArea: 386560,  includes: 6),
                new Format(name: "594 x 840",  width: 840,  height: 594, area: 498960, lowArea: 484720, upArea: 513400,  includes: 8),
                new Format(name: "594 x 1050", width: 1050, height: 594, area: 623700, lowArea: 607360, upArea: 640240,  includes: 10),
                new Format(name: "594 x 1260", width: 1260, height: 594, area: 748440, lowArea: 730000, upArea: 767080,  includes: 12),
                new Format(name: "594 x 1470", width: 1470, height: 594, area: 873180, lowArea: 852640, upArea: 893920,  includes: 14),
                new Format(name: "420 x 891",  width: 891,  height: 420, area: 374220, lowArea: 361210, upArea: 387430,  includes: 6),
                new Format(name: "420 x 1188", width: 1188, height: 420, area: 498960, lowArea: 482980, upArea: 515140,  includes: 8),
                new Format(name: "420 x 1485", width: 1485, height: 420, area: 623700, lowArea: 604750, upArea: 642850,  includes: 10)
            };

            // Занчения положения маркеров указаны в мм
            markers = new List<Marker>()
            {
                new Marker(x: 210, y: 150, position: MarkerPosition.Center),
                new Marker(x: 210, y: 30,  position: MarkerPosition.Top),
                new Marker(x: 55,  y: 30,  position: MarkerPosition.TopLeft),
                new Marker(x: 365, y: 30,  position: MarkerPosition.TopRight),
                new Marker(x: 55,  y: 240, position: MarkerPosition.BottomLeft),
                new Marker(x: 365, y: 240, position: MarkerPosition.BottomRight),
            };
        }

        public List<Format> Formats => formats;
        public List<Marker> Markers => markers;
    }
}
