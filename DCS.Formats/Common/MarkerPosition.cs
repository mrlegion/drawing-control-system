﻿namespace DCS.Helpers.Common
{
    public enum MarkerPosition
    {
        Center,
        TopLeft,
        TopRight,
        BottomLeft,
        BottomRight,
        Top,
    }
}