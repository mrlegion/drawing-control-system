﻿using System;
using System.IO;
using System.Windows;
using DCS.Logic;
using DCS.Logic.Common;
using DCS.Logic.Interface;
using Microsoft.WindowsAPICodePack.Dialogs;
using Prism.Commands;
using Prism.Mvvm;

namespace DCS.App.ViewModels
{
    public class MainViewModel : BindableBase
    {
        #region Fields

        private readonly IController controller;
        private bool isWatching = false;
        private bool autoStartStatus = true;
        private Visibility showDialog = Visibility.Hidden;
        private string dialogMessage = "";

        #endregion

        #region Construct

        public MainViewModel()
        {
            // Инициализация модели
            controller = new Controller();
            ((Controller) controller).PropertyChanged += (sender, args) => RaisePropertyChanged(args.PropertyName);

            // Инициализация команд
            CommandInit();

            controller.Logging($"Запуск программы: [ УСПЕШНО ]");
        }

        #endregion

        #region Properties

        public DirectoryInfo Directory
        {
            get => controller.Directory;
            set => controller.Directory = value;
        }

        public FileExtantion Extantion
        {
            get => controller.Extantion;
            set => controller.Extantion = value;
        }

        public bool IsWatching
        {
            get => isWatching;
            set => SetProperty(ref isWatching, value);
        }

        public Visibility ShowDialog
        {
            get => showDialog;
            set => SetProperty(ref showDialog, value);
        }

        public bool AutoStartStatus
        {
            get => autoStartStatus;
            set => SetProperty(ref autoStartStatus, value);
        }

        public string DialogMessage
        {
            get => dialogMessage;
            set => SetProperty(ref dialogMessage, value);
        }

        public string Log => controller.Log;

        public DelegateCommand StartWatchingCommand { get; private set; }

        public DelegateCommand StopWatchingCommand { get; private set; }

        public DelegateCommand OpenSelectDialogCommand { get; private set; }

        public DelegateCommand CloseWindowCommand { get; private set; }

        public DelegateCommand ShowInformationCommand { get; private set; }

        public DelegateCommand ExplodeDrawingCommand { get; private set; }

        public DelegateCommand SaveLogCommand { get; private set; }

        public DelegateCommand ChangeChapterCommand { get; private set; }

        public DelegateCommand CloseDialogWindowCommand { get; private set; }

        #endregion


        #region Init methods

        private void CommandInit()
        {
            OpenSelectDialogCommand = new DelegateCommand(() =>
            {
                CommonFileDialog dialog = new CommonOpenFileDialog()
                {
                    Multiselect = false,
                    IsFolderPicker = true
                };

                if (dialog.ShowDialog() == CommonFileDialogResult.Ok)
                {
                    Directory = new DirectoryInfo(dialog.FileName);
                    controller.Logging($"Выбрана новая директория для отслеживания: [ {Directory.Name} ]");
                }
                else return;

                if (AutoStartStatus)
                {
                    controller.RunWatcher();
                    IsWatching = true;
                }
            });

            StartWatchingCommand = new DelegateCommand(() =>
            {
                controller.RunWatcher();
                IsWatching = true;
            });

            StopWatchingCommand = new DelegateCommand(() =>
            {
                controller.StopWatcher();
                IsWatching = false;
            });

            ShowInformationCommand = new DelegateCommand(() =>
            {
                controller.ShowInformation(Information.Full);
            });

            ExplodeDrawingCommand = new DelegateCommand(() =>
            {
                controller.Explode();
            });

            SaveLogCommand = new DelegateCommand(() =>
            {
                if (Directory == null)
                    DialogMessage = "Не выбрана директория для отслеживания";
                else
                {
                    DialogMessage = $"Журнал событий сохранен в файл: {Environment.NewLine}[ {Path.Combine(Directory.FullName, "log.txt")} ]";
                    controller.SafeLogToFile();
                }

                ShowDialog = Visibility.Visible;
            });

            ChangeChapterCommand = new DelegateCommand(() =>
            {
                controller.ChangeChapter();
            });

            CloseDialogWindowCommand = new DelegateCommand(() => { ShowDialog = Visibility.Hidden; });

            CloseWindowCommand = new DelegateCommand(() =>
            {
                if (IsWatching)
                    controller.StopWatcher();

                Application.Current.ShutdownMode = ShutdownMode.OnExplicitShutdown;
                Application.Current.Shutdown(0);
            });
        }

        #endregion
    }
}