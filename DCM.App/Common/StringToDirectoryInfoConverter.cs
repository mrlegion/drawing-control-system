﻿using System;
using System.Globalization;
using System.IO;
using System.Windows;
using System.Windows.Data;

namespace DCS.App.Common
{
    public class StringToDirectoryInfoConverter : IValueConverter
    {
        /// <summary>Преобразует значение типа <see cref="T:System.String"/> в тип <see cref="System.IO.DirectoryInfo"/></summary>
        /// <param name="value">
        ///   Значение, произведенное исходной привязкой.
        /// </param>
        /// <param name="targetType">Тип целевого свойства привязки.</param>
        /// <param name="parameter">
        ///   Используемый параметр преобразователя.
        /// </param>
        /// <param name="culture">
        ///   Язык и региональные параметры, используемые в преобразователе.
        /// </param>
        /// <returns>
        ///   Преобразованное значение.
        ///    Если этот метод возвращает <see langword="null" />, используется допустимое значение NULL.
        /// </returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            DirectoryInfo di = value as DirectoryInfo;
            if (di == null)
                return DependencyProperty.UnsetValue;

            return di.FullName;
        }

        /// <summary>Преобразует значение <see cref="System.IO.DirectoryInfo"/> в тип <see cref="T:System.String"/> </summary>
        /// <param name="value">
        ///   Значение, произведенное целью привязки.
        /// </param>
        /// <param name="targetType">Целевой тип преобразования.</param>
        /// <param name="parameter">
        ///   Используемый параметр преобразователя.
        /// </param>
        /// <param name="culture">
        ///   Язык и региональные параметры, используемые в преобразователе.
        /// </param>
        /// <returns>
        ///   Преобразованное значение.
        ///    Если этот метод возвращает <see langword="null" />, используется допустимое значение NULL.
        /// </returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string path = value as string;
            if (string.IsNullOrEmpty(path) || string.IsNullOrWhiteSpace(path))
                return DependencyProperty.UnsetValue;

            return new DirectoryInfo(path);
        }
    }
}