﻿using System.IO;
using DCS.Logic.Common;

namespace DCS.Logic.Interface
{
    public interface IController : IMediator
    {
        /// <summary>
        /// Получение журнала событий
        /// </summary>
        string Log { get; }

        /// <summary>
        /// Получение или установка директории для отслеживания
        /// </summary>
        DirectoryInfo Directory { get; set; }

        /// <summary>
        /// Получение или установка расширения файлов для отслеживания
        /// </summary>
        FileExtantion Extantion { get; set; }

        /// <summary>
        /// Запуск отслеживания директории
        /// </summary>
        void RunWatcher();

        /// <summary>
        /// Остановка отслеживания директории
        /// </summary>
        void StopWatcher();

        /// <summary>
        /// Показать информацию о чертежах и папках
        /// </summary>
        /// <param name="type">Тип показываемой информации</param>
        void ShowInformation(Information type);

        /// <summary>
        /// Разбить найденные чертежи по форматам
        /// </summary>
        void Explode();

        /// <summary>
        /// Сохранить файл журнала событий в файл
        /// </summary>
        void SafeLogToFile();

        /// <summary>
        /// Смена главы папок в чертежах механически
        /// </summary>
        void ChangeChapter();
    }
}