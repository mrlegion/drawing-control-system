﻿using DCS.Logic.Common;
using DCS.Logic.Interface.Components;

namespace DCS.Logic.Interface
{
    public interface IMediator
    {
        /// <summary>
        /// Уведомление посредника о событии в компонентах
        /// </summary>
        /// <param name="component">Ссылка на компонент, который вызвал уведомление</param>
        /// <param name="event">Событие, которое вызывает компонет</param>
        /// <param name="message">Сообщение от компонента</param>
        void Notify(IComponent component = null, ComponentEvent @event = ComponentEvent.None, string message = "Запись в журнал событий");

        /// <summary>
        /// Отправка сообщения в журнал событий
        /// </summary>
        /// <param name="message">Собщение, которое отправляется в журнал событий</param>
        void Logging(string message);
    }
}