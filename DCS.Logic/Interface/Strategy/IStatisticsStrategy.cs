﻿using System.Collections.Generic;
using DCS.Logic.Common;

namespace DCS.Logic.Interface.Strategy
{
    public interface IStatisticsStrategy
    {
        /// <summary>
        /// Запуск процесса обработки колекции для получения инфомации
        /// </summary>
        /// <param name="collection">Исходная колекция, из которой требуется получить информацию</param>
        /// <returns></returns>
        string Proccess(IDictionary<string, List<Drawing>> collection);
    }
}