﻿using System.Collections.Generic;
using System.IO;
using DCS.Logic.Common;

namespace DCS.Logic.Interface.Components
{
    public interface IExploder : IComponent
    {
        /// <summary>
        /// Установка директории папки, в которую будет производиться деление по форматам
        /// </summary>
        DirectoryInfo Folder { set; }

        /// <summary>
        /// Деление переданной коллекции по форматам
        /// </summary>
        /// <param name="files">Коллекция чертежей для деления</param>
        void Explode(Dictionary<string, List<Drawing>> files);
    }
}