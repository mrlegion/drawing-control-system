﻿using System.Collections.Generic;
using DCS.Logic.Common;

namespace DCS.Logic.Interface.Components
{
    public interface IStoragekeeper : IComponent
    {
        /// <summary>
        /// Получение общего количества элементов в коллекции
        /// </summary>
        int Count { get; }

        /// <summary>
        /// Получение списка объектов чертежей
        /// </summary>
        List<Drawing> Drawings { get; }

        /// <summary>
        /// Дабавление объекта чертежа в коллекцию
        /// </summary>
        /// <param name="drawing">Ссылка на объект чертежа</param>
        void Add(Drawing drawing);

        /// <summary>
        /// Удаляет последний добавленный элемент
        /// </summary>
        void RemoveLast();

        /// <summary>
        /// Очистить список чертежей
        /// </summary>
        void Clear();
    }
}