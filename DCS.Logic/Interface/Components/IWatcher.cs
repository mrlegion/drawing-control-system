﻿using System.IO;
using DCS.Logic.Common;

namespace DCS.Logic.Interface.Components
{
    public interface IWatcher : IComponent
    {
        /// <summary>
        /// Получение информации о найденом файле
        /// </summary>
        FileInfo File { get; }

        /// <summary>
        /// Получение или установка директории для отслеживания
        /// </summary>
        DirectoryInfo Directory { get; set; }

        /// <summary>
        /// Получение или установка расширение файла для отслеживания, какие файлы должны быть отслеживаться
        /// </summary>
        FileExtantion Extantion { get; set; }

        /// <summary>
        /// Запуск отслеживания выбранной директории
        /// </summary>
        void Run();

        /// <summary>
        /// Запуск отслеживания выбранной директории
        /// </summary>
        /// <param name="directory">Директория, которую требуется отслеживать</param>
        /// <param name="extantion">Расширение файла, который требуется отслеживать</param>
        void Run(DirectoryInfo directory, FileExtantion extantion);

        /// <summary>
        /// Остановка отслеживания выбранной директории
        /// </summary>
        void Stop();
    }
}