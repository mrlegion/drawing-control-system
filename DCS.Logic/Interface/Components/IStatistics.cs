﻿using System.Collections.Generic;
using DCS.Logic.Common;
using DCS.Logic.Interface.Strategy;

namespace DCS.Logic.Interface.Components
{
    public interface IStatistics : IComponent
    {
        /// <summary>
        /// Получение или установка стратегии обработки информации
        /// </summary>
        IStatisticsStrategy Strategy { get; set; }

        /// <summary>
        /// Получение строки со статистической информацией
        /// </summary>
        /// <param name="collection">Исходная колекция, из которой требуется получить информацию</param>
        /// <returns></returns>
        string GetInformation(IDictionary<string, List<Drawing>> collection);
    }
}