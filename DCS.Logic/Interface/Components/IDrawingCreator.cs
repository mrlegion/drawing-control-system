﻿using System.IO;
using DCS.Logic.Common;
using DCS.Logic.Components;

namespace DCS.Logic.Interface.Components
{
    /// <summary>
    /// Компонент управления создания объектов <see cref="Drawing"/> для хранения информации о изображении чертежа
    /// </summary>
    public interface IDrawingCreator : IComponent
    {
        /// <summary>
        /// Получение или установка алгоритма для обработки изображения создания объекта чертежа типа <see cref="Drawing"/>
        /// </summary>
        CreateDrawingAlgorithm Strategy { get; set; }
        
        /// <summary>
        /// Получение объекта чертежа типа <see cref="Drawing"/>
        /// </summary>
        Drawing Image { get; }

        /// <summary>
        /// Создание нового объекта чертежа типа <see cref="Drawing"/>
        /// </summary>
        /// <param name="image">Путь к изовбражению в формате <see cref="FileInfo"/></param>
        /// <param name="type">Определения типа операции в создании нового экземпляра класса <see cref="Drawing"/></param>
        /// <returns>Объекти типа <see cref="Drawing"/></returns>
        void Create(FileInfo image, CreateType type = CreateType.Create);
    }
}