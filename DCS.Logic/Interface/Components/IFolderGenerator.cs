﻿using System.IO;

namespace DCS.Logic.Interface.Components
{
    public interface IFolderGenerator : IComponent
    {
        /// <summary>
        /// Получение информации о новой директории
        /// </summary>
        DirectoryInfo Directory { get; }

        /// <summary>
        /// Созданить новую директорию
        /// </summary>
        /// <param name="source">Ссылка на базовую директорию отслеживания</param>
        void Create(DirectoryInfo source);
    }
}