﻿namespace DCS.Logic.Interface.Components
{
    public interface IComponent
    {
        /// <summary>
        /// Получение или установка на посредника компонентов
        /// </summary>
        IMediator Mediator { get; set; }
    }
}