﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DCS.Logic.Common;

namespace DCS.Logic.Interface.Components
{
    public interface IManager : IComponent
    {
        /// <summary>
        /// Получение упорядоченной инфомации о всех чертежах
        /// </summary>
        Dictionary<string, List<Drawing>> Data { get; }

        /// <summary>
        /// Доавление новой главы чертежей в список
        /// </summary>
        /// <param name="name">Наименование главы</param>
        /// <param name="list">Коллекция чертежей</param>
        void AddChapter(string name, List<Drawing> list);
    }
}
