﻿using System.Collections.Generic;
using System.IO;
using DCS.Logic.Common;

namespace DCS.Logic.Interface.Components
{
    public interface IFileCarrier : IComponent
    {
        /// <summary>
        /// Получение или установка директории, в которую требуется переместить файлы
        /// </summary>
        DirectoryInfo To { get; set; }

        /// <summary>
        /// Получение или установка списка файлов для перемещения
        /// </summary>
        List<Drawing> Files { get; set; }

        /// <summary>
        /// Получение или установка типа для перемещения файлов
        /// </summary>
        TransferType TransferType { get; set; }
        
        /// <summary>
        /// Перемещение файлов в указанную директорию
        /// </summary>
        void Transfer();

        /// <summary>
        /// Перемещение файлов в указанную директорию
        /// </summary>
        /// <param name="to">Директория, в которую требуется переместить файлы</param>
        void Transfer(DirectoryInfo to);

        /// <summary>
        /// Перемещение файлов в указанную директорию
        /// </summary>
        /// <param name="to">Директория, в которую требуется переместить файлы</param>
        /// <param name="collection">Списк файлов для перемещения</param>
        void Transfer(DirectoryInfo to, List<Drawing> collection);

        /// <summary>
        /// Очистка списка файлов для перемещения
        /// </summary>
        void ClearTransferList();
    }
}