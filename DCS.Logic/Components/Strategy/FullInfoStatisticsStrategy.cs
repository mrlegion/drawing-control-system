﻿using System;
using System.Collections.Generic;
using System.IO.Packaging;
using System.Linq;
using System.Text;
using DCS.Logic.Common;
using DCS.Logic.Interface.Strategy;

namespace DCS.Logic.Components.Strategy
{
    public class FullInfoStatisticsStrategy : IStatisticsStrategy
    {
        // Todo: Реализовать более красивый вид вывода информации
        /// <summary>
        /// Запуск процесса обработки колекции для получения инфомации
        /// </summary>
        /// <param name="collection">Исходная колекция, из которой требуется получить информацию</param>
        /// <returns></returns>
        public string Proccess(IDictionary<string, List<Drawing>> collection)
        {
            StringBuilder builder = new StringBuilder();
            double count = 0;
            foreach (var folder in collection)
            {
                builder.Append('-', 30);
                builder.Append($"{Environment.NewLine}Наименование папки: {folder.Key}");
                builder.Append($"{Environment.NewLine}В папке находятся {folder.Value.Count} чертежей:");
                builder.Append($"{Environment.NewLine}Список форматов:");
                builder.Append(Environment.NewLine);

                var formats = folder.Value.GroupBy(d => d.Format);

                foreach (IGrouping<string, Drawing> format in formats)
                {
                    builder.AppendFormat("{0, -10} : {1, -10}", format.Key, format.Count());
                    builder.Append(Environment.NewLine);
                }

                count += folder.Value.Sum(d => d.Include);

                builder.Append(Environment.NewLine);
                builder.AppendFormat("Общее количество в а4: {0, -5}", folder.Value.Sum(d => d.Include));
                builder.Append(Environment.NewLine);
                builder.Append('-', 30);
            }

            builder.AppendFormat($"{Environment.NewLine}ОБЩЕЕ КОЛ-ВО А4: {count, -5}");
            builder.Append(Environment.NewLine);

            return builder.ToString();
        }
    }
}