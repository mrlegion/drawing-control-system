﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using DCS.Logic.Common;
using DCS.Logic.Interface;
using DCS.Logic.Interface.Components;

namespace DCS.Logic.Components
{
    public class FileCarrier : IFileCarrier
    {
        private List<Drawing> files;
        /// <summary>
        /// Получение или установка на посредника компонентов
        /// </summary>
        public IMediator Mediator { get; set; }

        /// <summary>
        /// Получение или установка директории, в которую требуется переместить файлы
        /// </summary>
        public DirectoryInfo To { get; set; }

        /// <summary>
        /// Получение или установка списка файлов для перемещения
        /// </summary>
        public List<Drawing> Files
        {
            get => files;
            set => files = new List<Drawing>(value);
        }

        /// <summary>
        /// Получение или установка типа для перемещения файлов
        /// </summary>
        public TransferType TransferType { get; set; }

        /// <summary>
        /// Перемещение файлов в указанную директорию
        /// </summary>
        public void Transfer()
        {
            foreach (Drawing drawing in Files)
            {
                string fileName = Path.GetFileName(drawing.Path.FullName);
                string newPath = Path.Combine(To.FullName, fileName);

                File.Move(drawing.Path.FullName, newPath);
                if (File.Exists(newPath))
                {
                    drawing.Path = new FileInfo(newPath);
                    Mediator.Logging($"Файл [{drawing.Path.Name}] был перемещен");
                }
                else
                {
                    Mediator.Notify(component: this, @event: ComponentEvent.FileIsNotExist, message: $"Перемещенный файл не найден [{newPath}]");
                    return;
                }
            }

            Mediator.Notify(component: this, @event: ComponentEvent.MovingDone, message: $"Перемещение файлов в папку {To.Name} завершенно");
        }

        /// <summary>
        /// Перемещение файлов в указанную директорию
        /// </summary>
        /// <param name="to">Директория, в которую требуется переместить файлы</param>
        public void Transfer(DirectoryInfo to)
        {
            if (!to.Exists)
            {
                Mediator.Notify(component: this, @event: ComponentEvent.DirectoryIsNotExist, message: $"Директория [{to.FullName}] не существует");
                return;
            }

            To = to;
            Transfer();
        }

        /// <summary>
        /// Перемещение файлов в указанную директорию
        /// </summary>
        /// <param name="to">Директория, в которую требуется переместить файлы</param>
        /// <param name="collection">Списк файлов для перемещения</param>
        public void Transfer(DirectoryInfo to, List<Drawing> collection)
        {
            if (!collection.Any())
            {
                Mediator.Notify(component: this, @event: ComponentEvent.CollectionIsEmpty, message: $"Переданный список пуст");
                return;
            }

            Files = collection;
            Transfer(to);
        }

        /// <summary>
        /// Очистка списка файлов для перемещения
        /// </summary>
        public void ClearTransferList()
        {
            if (Files.Count > 0)
                Files.Clear();
        }
    }
}