﻿using System;
using System.IO;
using DCS.Logic.Common;
using DCS.Logic.Interface;
using DCS.Logic.Interface.Components;

namespace DCS.Logic.Components
{
    /// <summary>
    /// Компонент управления создания объектов <see cref="Drawing"/> для хранения информации о изображении чертежа
    /// </summary>
    public class DrawingCreator : IDrawingCreator
    {
        /// <summary>
        /// Инициализация нового экземпляра класса <see cref="DrawingCreator"/>
        /// </summary>
        public DrawingCreator() { }

        /// <summary>
        /// Получение или установка на посредника компонентов
        /// </summary>
        public IMediator Mediator { get; set; }

        /// <summary>
        /// Получение объекта чертежа типа <see cref="Drawing"/>
        /// </summary>
        public Drawing Image { get; private set; }

        /// <summary>
        /// Получение или установка алгоритма для обработки изображения создания объекта чертежа типа <see cref="Drawing"/>
        /// </summary>
        public CreateDrawingAlgorithm Strategy { get; set; }

        /// <summary>
        /// Создание нового объекта чертежа типа <see cref="Drawing"/>
        /// </summary>
        /// <param name="image">Путь к изовбражению в формате <see cref="FileInfo"/></param>
        /// <param name="type">Определения типа операции в создании нового экземпляра класса <see cref="Drawing"/></param>
        /// <returns>Объекти типа <see cref="Drawing"/></returns>
        public void Create(FileInfo image, CreateType type)
        {
            try
            {
                Image = Strategy.Proccess(file: image);
            }
            catch (Exception e)
            {
                Mediator.Logging($"Во время создания чертежа произошла ошибка! {e.Message}");
                return;
            }

            if (Image.IsMarker)
            {

                Mediator.Notify(component: this,
                    @event: ComponentEvent.MarkerFound,
                    message: $"Обнаружен маркер: [{image.Name}]! Осуществляю переход к следующей части");
                return;
            }


            switch (type)
            {
                case CreateType.Modify:
                    Mediator.Notify(component: this,
                        @event: ComponentEvent.FileIsModify,
                        message:
                        $"Изображение [{image.Name}] переданно кладавщику. Формат изображения [{Image.Format}]");
                    break;
                case CreateType.Create:
                default:
                    Mediator.Notify(component: this,
                        @event: ComponentEvent.DrawingIsCreate,
                        message:
                        $"Изображение [{image.Name}] переданно кладавщику. Формат изображения [{Image.Format}]");
                    break;
            }
        }
    }
}