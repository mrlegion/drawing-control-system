﻿using System.Drawing;

namespace DCS.Logic.Components
{
    public class JpegAlgorithm : CreateDrawingAlgorithm
    {
        /// <summary>
        /// Описание метода открытия изображения 
        /// </summary>
        protected override void OpenImage()
        {
            bitmap = new Bitmap(file.FullName);
        }
    }
}