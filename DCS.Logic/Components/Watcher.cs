﻿using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading;
using DCS.Logic.Common;
using DCS.Logic.Interface;
using DCS.Logic.Interface.Components;

namespace DCS.Logic.Components
{
    public class Watcher : IWatcher
    {
        #region Fields

        /// <summary>
        /// Ссылка на смотрителя файловой системы, который осуществляет отслеживание выбранной директории
        /// </summary>
        private FileSystemWatcher watcher;

        #endregion

        #region Construct

        /// <summary>
        /// Осуществляет инициализацию нового экзеспляра класса <see cref="Watcher"/>
        /// </summary>
        public Watcher()
        {
            InitFileSystemWatcher();
        }

        #endregion

        #region Properties

        /// <summary>
        /// Получение или установка на посредника компонентов
        /// </summary>
        public IMediator Mediator { get; set; }

        /// <summary>
        /// Получение информации о найденом файле
        /// </summary>
        public FileInfo File { get; private set; }

        /// <summary>
        /// Получение или установка директории для отслеживания
        /// </summary>
        public DirectoryInfo Directory { get; set; }

        /// <summary>
        /// Получение или установка расширение файла для отслеживания, какие файлы должны быть отслеживаться
        /// </summary>
        public FileExtantion Extantion { get; set; }

        #endregion

        #region Events

        private void WatcherCreateEventHandler(object sender, FileSystemEventArgs e)
        {
            if (!CheckedExtantion(foundExtantion: Path.GetExtension(e.FullPath)))
                return;

            if (File != null && File.Name == e.FullPath)
                Mediator.Notify(component: this,
                                @event: ComponentEvent.FileIsModify,
                                message: $"Файл [{File?.Name}] был изменен!");
            else
            {

                File = new FileInfo(e.FullPath);
                Mediator.Notify(component: this, 
                                @event: ComponentEvent.FileIsFound, 
                                message: $"Файл создан с выбранным расширением {Extantion}");
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Запуск отслеживания выбранной директории
        /// </summary>
        public void Run()
        {
            if (!Directory.Exists)
            {
                Mediator.Notify(component: this, 
                                @event: ComponentEvent.DirectoryIsNotExist, 
                                message: $"Выбранная директория: [{Directory.FullName}] не найдена");
                return;
            }

            watcher.Path = Directory.FullName;
            watcher.EnableRaisingEvents = true;
        }

        /// <summary>
        /// Запуск отслеживания выбранной директории
        /// </summary>
        /// <param name="directory">Директория, которую требуется отслеживать</param>
        /// <param name="extantion">Расширение файла, который требуется отслеживать</param>
        public void Run(DirectoryInfo directory, FileExtantion extantion)
        {
            Directory = directory;
            Extantion = extantion;
            Run();
        }

        /// <summary>
        /// Остановка отслеживания выбранной директории
        /// </summary>
        public void Stop()
        {
            watcher.EnableRaisingEvents = false;
        }

        #endregion

        #region Private methods

        /// <summary>
        /// Инициализация файлового смотрителя
        /// </summary>
        private void InitFileSystemWatcher()
        {
            watcher = new FileSystemWatcher()
            {
                Filter = "*.*",
                IncludeSubdirectories = false,
                NotifyFilter = NotifyFilters.FileName | NotifyFilters.DirectoryName | NotifyFilters.CreationTime,
            };

            watcher.Created += WatcherCreateEventHandler;
        }

        /// <summary>
        /// Проверка расширения найденного файла
        /// </summary>
        /// <param name="foundExtantion">Путь до найденного файла</param>
        /// <returns>Подходит ли данный файл по требованиям</returns>
        private bool CheckedExtantion(string foundExtantion)
        {
            bool result = false;

            switch (Extantion)
            {
                case FileExtantion.Tiff:
                    result = Regex.IsMatch(foundExtantion, "tiff$|tif$", RegexOptions.IgnoreCase);
                    break;
                case FileExtantion.Pdf:
                    result = Regex.IsMatch(foundExtantion, "pdf$", RegexOptions.IgnoreCase);
                    break;
                case FileExtantion.Jpeg:
                    result = Regex.IsMatch(foundExtantion, "jpeg$|jpg$", RegexOptions.IgnoreCase);
                    break;
            }

            return result;
        }

        #endregion


    }
}