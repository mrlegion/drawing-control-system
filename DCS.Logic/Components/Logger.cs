﻿using System;
using System.IO;
using System.Text;
using DCS.Logic.Common;
using DCS.Logic.Interface;
using DCS.Logic.Interface.Components;
using Prism.Mvvm;

namespace DCS.Logic.Components
{
    /// <summary>
    /// Компонент управления журналом событий
    /// </summary>
    public class Logger : BindableBase, ILogger
    {
        private string log;

        public string Log
        {
            get => log;
            set => SetProperty(ref log, value);
        }

        /// <summary>
        /// Инициализация нового экземпляра класса <see cref="Logger"/>
        /// </summary>
        public Logger() { }

        /// <summary>
        /// Получение или установка на посредника компонентов
        /// </summary>
        public IMediator Mediator { get; set; }

        /// <summary>
        /// Вывод информации о событиях в программе
        /// </summary>
        /// <param name="message">Сообщение, которое следует вывести пользователю</param>
        public void Logging(string message)
        {
            StringBuilder sb = new StringBuilder(Log);
            sb.Append(Environment.NewLine);
            sb.AppendFormat("{0:T}: {1}", DateTime.Now, message);
            Log = sb.ToString();
        }

        /// <summary>
        /// Сохранить файл журнала событий в файл
        /// </summary>
        /// <param name="directory">Директория, в которую следует сохранить файл журнала событий</param>
        public void SaveToFile(DirectoryInfo directory)
        {
            if (!directory.Exists)
            {
                Mediator.Logging($"Выбранная директория для сохранения журнала событий не существует!");
                return;
            }

            string file = Path.Combine(directory.FullName, "log.txt");

            if (File.Exists(file))
                File.Delete(file);

            Stream stream = File.Create(file);
            StreamWriter writer = new StreamWriter(stream);
            writer.Write(log);
            writer.Close();
            stream.Close();
        }
    }
}