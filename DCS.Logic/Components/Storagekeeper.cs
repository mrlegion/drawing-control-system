﻿using System.Collections.Generic;
using DCS.Logic.Common;
using DCS.Logic.Interface;
using DCS.Logic.Interface.Components;

namespace DCS.Logic.Components
{
    public class Storagekeeper : IStoragekeeper
    {
        /// <summary>
        /// Получение или установка на посредника компонентов
        /// </summary>
        public IMediator Mediator { get; set; }


        /// <summary>
        /// Получение общего количества элементов в коллекции
        /// </summary>
        public int Count => Drawings.Count;

        /// <summary>
        /// Получение списка объектов чертежей
        /// </summary>
        public List<Drawing> Drawings { get; } = new List<Drawing>();

        /// <summary>
        /// Дабавление объекта чертежа в коллекцию
        /// </summary>
        /// <param name="drawing">Ссылка на объект чертежа</param>
        public void Add(Drawing drawing)
        {
            Drawings.Add(drawing);
            Mediator.Logging($"Чертеж добавлен в хранилище");
            Mediator.Logging($"Общее количество чертежей в части: {Count}");
        }

        /// <summary>
        /// Удаляет последний добавленный элемент
        /// </summary>
        public void RemoveLast()
        {
            Drawings.RemoveAt(Count - 1);
        }

        /// <summary>
        /// Очистить список чертежей
        /// </summary>
        public void Clear()
        {
            Drawings.Clear();
            Mediator.Logging($"Хранилище очищенно");
        }
    }
}