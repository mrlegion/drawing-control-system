﻿using System;
using System.Drawing;

namespace DCS.Logic.Components
{
    public class TiffAlgorithm : CreateDrawingAlgorithm
    {
        /// <summary>
        /// Описание метода открытия изображения 
        /// </summary>
        protected override void OpenImage()
        {
            try
            {
                bitmap = new Bitmap(file.FullName);
            }
            catch (Exception e)
            {
                bitmap = null;
                throw new Exception(e.Message, e);
            }
        }
    }
}