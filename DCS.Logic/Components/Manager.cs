﻿using DCS.Logic.Interface;
using DCS.Logic.Interface.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DCS.Logic.Common;

namespace DCS.Logic.Components
{
    public class Manager : IManager
    {
        private readonly Dictionary<string, List<Drawing>> data;

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="T:DCS.Logic.Components.Manager" />.
        /// </summary>
        public Manager()
        {
            data = new Dictionary<string, List<Drawing>>();
        }

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="T:DCS.Logic.Components.Manager" />.
        /// </summary>
        /// <param name="mediator">Ссылка на посредника</param>
        public Manager(IMediator mediator) : this ()
        {
            Mediator = mediator;
        }

        /// <summary>
        /// Получение или установка на посредника компонентов
        /// </summary>
        public IMediator Mediator { get; set; }

        /// <summary>
        /// Получение упорядоченной инфомации о всех чертежах
        /// </summary>
        public Dictionary<string, List<Drawing>> Data => data;

        /// <summary>
        /// Доавление новой главы чертежей в список
        /// </summary>
        /// <param name="name">Наименование главы</param>
        /// <param name="list">Коллекция чертежей</param>
        public void AddChapter(string name, List<Drawing> list)
        {
            // Todo: Сделать генерацию случайного имени для главы
            if (string.IsNullOrEmpty(name) || string.IsNullOrWhiteSpace(name))
            {
                Mediator.Logging(message: $"Создано случайно сгенерированное имя для папки");
                return;
            }

            if (list == null)
            {
                Mediator.Notify(@event: ComponentEvent.CollectionIsEmpty, 
                                message: $"Невозможно добавить папку в список, так как коллекция пустая");
                return;
            }
            
            if (data.ContainsKey(key: name))
            {
                data[name].AddRange(list);
                return;
            }

            data.Add(key: name, value: list);
            Mediator.Logging($"Папка [{name}] добавлена в хранилище и готова к обработке");
        }
    }
}
