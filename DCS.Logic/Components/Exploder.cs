﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using DCS.Logic.Common;
using DCS.Logic.Interface;
using DCS.Logic.Interface.Components;

namespace DCS.Logic.Components
{
    public class Exploder : IExploder
    {
        /// <summary>
        /// Получение или установка на посредника компонентов
        /// </summary>
        public IMediator Mediator { get; set; }

        /// <summary>
        /// Установка наименование папки, в которую будет производиться деление по форматам
        /// </summary>
        public DirectoryInfo Folder { private get; set; }

        // Todo: Сделать деление по нескольним принципам
        /// <summary>
        /// Деление переданной коллекции по форматам
        /// </summary>
        /// <param name="files">Коллекция чертежей для деления</param>
        public void Explode(Dictionary<string, List<Drawing>> files)
        {
            if (files == null || files.Count == 0)
            {
                Mediator.Notify(@event: ComponentEvent.CollectionIsEmpty,
                                message: $"Коллекция для деления по форматам пустая");
                return;
            }
            
            if (!Folder.Exists) Folder.Create();

            // Сортировка по формату
            Dictionary<string, List<Drawing>> groups = new Dictionary<string, List<Drawing>>();

            foreach (KeyValuePair<string, List<Drawing>> folder in files)
            {
                foreach (Drawing drawing in folder.Value)
                {
                    if (groups.ContainsKey(drawing.Format))
                        groups[drawing.Format].Add(drawing);
                    else
                        groups.Add(drawing.Format, new List<Drawing> {drawing});
                }
            }

            // Перемещение файлов
            foreach (var format in groups)
            {
                string subfolder = Path.Combine(Folder.FullName, format.Key);
                Folder.CreateSubdirectory(format.Key);

                format.Value.ForEach(d => Moving(subfolder, d));

                Mediator.Logging($"Формат чертежей {format.Key} перемещен в директорию [{subfolder}]");
            }

            Mediator.Logging($"Разбитие чертежей завершенно");
        }

        private void Moving(string to, Drawing drawing)
        {
            string filename = drawing.Path.Name;
            File.Move(drawing.Path.FullName, Path.Combine(to, filename));
        }
    }
}