﻿using System.IO;
using DCS.Logic.Common;
using DCS.Logic.Interface;
using DCS.Logic.Interface.Components;

namespace DCS.Logic.Components
{
    public class FolderGenerator : IFolderGenerator
    {
        /// <summary>
        /// Получение или установка на посредника компонентов
        /// </summary>
        public IMediator Mediator { get; set; }

        /// <summary>
        /// Получение информации о новой директории
        /// </summary>
        public DirectoryInfo Directory { get; private set; }

        /// <summary>
        /// Созданить новую директорию
        /// </summary>
        /// <param name="source">Ссылка на базовую директорию отслеживания</param>
        public void Create(DirectoryInfo source)
        {
            if (!source.Exists) throw new DirectoryNotFoundException();

            int count = 0;

            string newDirectory;

            do
            {
                count++;
                newDirectory = Path.Combine(source.FullName, count.ToString());
            } while (System.IO.Directory.Exists(newDirectory));

            Directory = new DirectoryInfo(newDirectory);
            Directory.Create();

            Mediator.Notify(component: this, @event: ComponentEvent.DirectoryCreated, message: $"Директория [{Directory.FullName}] была создана");
        }
    }
}