﻿using System.Collections.Generic;
using DCS.Logic.Common;
using DCS.Logic.Components.Strategy;
using DCS.Logic.Interface;
using DCS.Logic.Interface.Components;
using DCS.Logic.Interface.Strategy;

namespace DCS.Logic.Components
{
    public class Statistics : IStatistics
    {
        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="T:DCS.Logic.Components.Statistics" />.
        /// </summary>
        public Statistics() { }

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="T:DCS.Logic.Components.Statistics" />.
        /// </summary>
        public Statistics(IMediator mediator)
        {
            Mediator = mediator;
        }

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="T:DCS.Logic.Components.Statistics" />.
        /// </summary>
        public Statistics(IMediator mediator, IStatisticsStrategy strategy)
        {
            Mediator = mediator;
            Strategy = strategy;
        }

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="T:DCS.Logic.Components.Statistics" />.
        /// </summary>
        public Statistics(IStatisticsStrategy strategy)
        {
            Strategy = strategy;
        }

        /// <summary>
        /// Получение или установка на посредника компонентов
        /// </summary>
        public IMediator Mediator { get; set; }

        /// <summary>
        /// Получение или установка стратегии обработки информации
        /// </summary>
        public IStatisticsStrategy Strategy { get; set; }

        /// <summary>
        /// Получение строки со статистической информацией
        /// </summary>
        /// <param name="collection">Исходная колекция, из которой требуется получить информацию</param>
        /// <returns></returns>
        public string GetInformation(IDictionary<string, List<Drawing>> collection)
        {
            if (Strategy == null)
                Strategy = new FullInfoStatisticsStrategy();

            return Strategy.Proccess(collection: collection);
        }
    }
}