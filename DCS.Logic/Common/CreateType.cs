﻿namespace DCS.Logic.Common
{
    public enum CreateType
    {
        None,
        Create,
        Modify
    }
}