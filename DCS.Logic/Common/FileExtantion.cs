﻿namespace DCS.Logic.Common
{
    public enum FileExtantion
    {
        None,
        Tiff,
        Pdf,
        Jpeg,
    }
}