﻿namespace DCS.Logic.Common
{
    public enum TransferType
    {
        /// <summary>
        /// Ничего не делать, нулевое значение
        /// </summary>
        None,

        /// <summary>
        /// Перемещать файлы
        /// </summary>
        Move,

        /// <summary>
        /// Файлы только копировать
        /// </summary>
        Copy,
    }
}