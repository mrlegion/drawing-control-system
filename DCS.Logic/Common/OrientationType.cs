﻿namespace DCS.Logic.Common
{
    public enum OrientationType
    {
        Vertical,
        Horizontal
    }
}