﻿namespace DCS.Logic.Common
{
    public enum FileExistBehavior
    {
        None,
        Replace,
        Skip,
        Rename
    }
}