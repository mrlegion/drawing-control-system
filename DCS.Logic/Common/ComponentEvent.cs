﻿namespace DCS.Logic.Common
{
    public enum ComponentEvent
    {
        /// <summary>
        /// Пустое событие
        /// </summary>
        None,

        /// <summary>
        /// Выбранная директория не найдена
        /// </summary>
        DirectoryIsNotExist,

        /// <summary>
        /// Выбранный или переданный файл не найден
        /// </summary>
        FileIsNotExist,

        /// <summary>
        /// Переданная коллекция пустая или является Null
        /// </summary>
        CollectionIsEmpty,

        /// <summary>
        /// Найден файл по выбранному фильтру
        /// </summary>
        FileIsFound,

        /// <summary>
        /// Ранее созданный файл был изменен
        /// </summary>
        FileIsModify,

        /// <summary>
        /// Создан новый экземпляр класса <see cref="Drawing"/>
        /// </summary>
        DrawingIsCreate,

        /// <summary>
        /// Найдено изображение маркера
        /// </summary>
        MarkerFound,

        /// <summary>
        /// Новая директория создана
        /// </summary>
        DirectoryCreated,

        /// <summary>
        /// Перемещение файлов произведенно успешно
        /// </summary>
        MovingDone,
    }
}