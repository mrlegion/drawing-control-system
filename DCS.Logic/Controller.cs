﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using DCS.Logic.Common;
using DCS.Logic.Components;
using DCS.Logic.Components.Strategy;
using DCS.Logic.Interface;
using DCS.Logic.Interface.Components;
using Prism.Mvvm;

namespace DCS.Logic
{
    public class Controller : BindableBase, IController
    {
        #region Components

        private FileExtantion extantion;
        private CreateDrawingAlgorithm strategy;
        private DirectoryInfo directory;

        private readonly IWatcher watcher;
        private readonly ILogger logger;
        private readonly IDrawingCreator creator;
        private readonly IStoragekeeper storagekeeper;
        private readonly IFolderGenerator folderGenerator;
        private readonly IFileCarrier carrier;
        private readonly IManager manager;
        private readonly IStatistics statistics;
        private readonly IExploder exploder;

        #endregion

        #region Construct

        /// <summary>
        /// Инициализация нового экземпляра класса <see cref="Controller"/>
        /// </summary>
        public Controller()
        {
            // Инициализация компонентов и передача ссылки им на посредника IMediator
            watcher = new Watcher() { Mediator = this };

            logger = new Logger() { Mediator = this };
            ((Logger) logger).PropertyChanged += (sender, args) => RaisePropertyChanged(args.PropertyName);

            creator = new DrawingCreator() { Mediator = this };
            storagekeeper = new Storagekeeper() { Mediator = this };
            folderGenerator = new FolderGenerator() { Mediator = this };
            carrier = new FileCarrier() { Mediator = this };
            manager = new Manager() { Mediator = this };
            statistics = new Statistics() { Mediator = this };
            exploder = new Exploder() { Mediator = this };
        }

        #endregion

        #region Properties

        /// <summary>
        /// Получение или установка директории для отслеживания
        /// </summary>
        public DirectoryInfo Directory
        {
            get => directory;
            set => SetProperty(ref directory, value);
        }

        /// <summary>
        /// Получение журнала событий
        /// </summary>
        public string Log => logger.Log;

        /// <summary>
        /// Получение или установка расширения файлов для отслеживания
        /// </summary>
        public FileExtantion Extantion
        {
            get => extantion;
            set
            {
                switch (value)
                {
                    case FileExtantion.Tiff:
                        strategy = new TiffAlgorithm();
                        break;
                    case FileExtantion.Pdf:
                        strategy = null;
                        break;
                    case FileExtantion.Jpeg:
                        strategy = new JpegAlgorithm();
                        break;
                }

                SetProperty(ref extantion, value);
            }
        }

        #endregion

        /// <summary>
        /// Уведомление посредника о событии в компонентах
        /// </summary>
        /// <param name="component">Ссылка на компонент, который вызвал уведомление</param>
        /// <param name="event">Событие, которое вызывает компонет</param>
        /// <param name="message">Сообщение от компонента</param>
        public void Notify(IComponent component = null, ComponentEvent @event = ComponentEvent.None, string message = "Запись в журнал событий")
        {
            logger.Logging(message);

            switch (@event)
            {
                case ComponentEvent.FileIsFound:
                    FoundFileEventHandler(component);
                    break;
                case ComponentEvent.FileIsModify:
                    FileModifyEventHandler(component);
                    break;
                case ComponentEvent.DrawingIsCreate:
                    DrawingCreateEventHandler(component);
                    break;
                case ComponentEvent.MarkerFound:
                    FoundMarkerEventHandler(component);
                    break;
                case ComponentEvent.DirectoryCreated:
                    DirectoryCreatedEventHandler(component);
                    break;
                case ComponentEvent.MovingDone:
                    MovingDoneEventHandler(component);
                    break;
            }
        }

        #region Events Handler

        /// <summary>
        /// Событие вызываемое при создании новой директории
        /// </summary>
        /// <param name="component">Компонент, который вызывает событие</param>
        private void DirectoryCreatedEventHandler(IComponent component)
        {
            carrier.Files = storagekeeper.Drawings;
            carrier.To = ((IFolderGenerator)component).Directory;
            carrier.TransferType = TransferType.Move;
            carrier.Transfer();
            storagekeeper.Clear();
        }

        /// <summary>
        /// Событие вызываемое при нахождении маркера
        /// </summary>
        /// <param name="component">Компонент, который вызывает событие</param>
        private void FoundMarkerEventHandler(IComponent component)
        {
            ((IDrawingCreator)component).Image.Path.Delete();
            folderGenerator.Create(Directory);
        }

        /// <summary>
        /// Событие вызываемое при завершении создания нового экземпляра класса <see cref="Drawing"/>
        /// </summary>
        /// <param name="component">Компонент, который вызывает событие</param>
        private void DrawingCreateEventHandler(IComponent component)
        {
            if (component is IDrawingCreator c)
                storagekeeper.Add(c.Image);
        }

        /// <summary>
        /// Событие вызываемое при нахождении файла компонентом <see cref="IWatcher"/>
        /// </summary>
        /// <param name="component">Компонент, который вызывает событие</param>
        private void FoundFileEventHandler(IComponent component)
        {
            // Todo: убрать при подключении WPF
            Thread.Sleep(1000);

            if (creator.Strategy == null)
                creator.Strategy = strategy;

            creator.Create(((IWatcher)component).File);
        }

        // Todo: подумать как это убрать
        /// <summary>
        /// Событие вызываемое при определении изменения файла
        /// </summary>
        /// <param name="component">Компонент, который вызывает событие</param>
        private void FileModifyEventHandler(IComponent component)
        {
            if (component is IWatcher w)
            {
                // Todo: убрать при подключении WPF
                Thread.Sleep(1000);

                if (creator.Strategy == null)
                    creator.Strategy = strategy;

                creator.Create(((IWatcher)component).File, CreateType.Modify);
            }

            if (component is IDrawingCreator dc)
            {
                storagekeeper.RemoveLast();
                storagekeeper.Add(dc.Image);
            }
        }

        /// <summary>
        /// Событие вызываемое при завершении перемещения файлов в новую директорию
        /// </summary>
        /// <param name="component">Компонент, который вызывает событие</param>
        private void MovingDoneEventHandler(IComponent component)
        {
            manager.AddChapter(carrier.To.Name, new List<Drawing>(carrier.Files));
            carrier.ClearTransferList();
        }

        #endregion

        /// <summary>
        /// Отправка сообщения в журнал событий
        /// </summary>
        /// <param name="message">Собщение, которое отправляется в журнал событий</param>
        public void Logging(string message)
        {
            logger.Logging(message);
        }

        /// <summary>
        /// Запуск отслеживания директории
        /// </summary>
        public void RunWatcher()
        {
            Logging($"Начало отслеживания директории: [ {Directory.Name} ]");
            watcher.Directory = Directory;
            watcher.Extantion = FileExtantion.Tiff;
            watcher.Run();
        }

        /// <summary>
        /// Остановка отслеживания директории
        /// </summary>
        public void StopWatcher()
        {
            Logging($"Окончание отслеживания директории: [ {Directory.Name} ]");
            watcher.Stop();
        }

        /// <summary>
        /// Показать информацию о чертежах и папках
        /// </summary>
        /// <param name="type">Тип показываемой информации</param>
        public void ShowInformation(Information type)
        {
            switch (type)
            {
                case Information.Full:
                    statistics.Strategy = new FullInfoStatisticsStrategy();
                    break;
            }

            string result = statistics.GetInformation(manager.Data);

            Logging(result);
        }

        /// <summary>
        /// Разбить найденные чертежи по форматам
        /// </summary>
        public void Explode()
        {
            if (manager.Data.Count == 0)
            {
                Logging("В коллекции нет ни одного файла! Для деления по форматам добавте файлы!");
                return;
            }

            string explodeDirectory = Path.Combine(Directory.FullName, "Explode Folder");
            exploder.Folder = new DirectoryInfo(explodeDirectory);
            exploder.Explode(manager.Data);
        }

        /// <summary>
        /// Сохранить файл журнала событий в файл
        /// </summary>
        public void SafeLogToFile()
        {
            logger.SaveToFile(Directory);
        }

        /// <summary>
        /// Смена главы папок в чертежах механически
        /// </summary>
        public void ChangeChapter()
        {
            if (storagekeeper.Count != 0)
            {
                folderGenerator.Create(Directory);
                return;
            }
                
            Logging("В коллекции нет ни одного файла! Для начала деления по главам добавте файлы!");
        }
    }
}
