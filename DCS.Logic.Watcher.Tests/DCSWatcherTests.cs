﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DCS.Logic.Common;
using DCS.Logic.Interface.Components;
using NUnit.Framework;

namespace DCS.Logic.Watcher.Tests
{
    [TestFixture]
    public class DcsWatcherTests
    {
        [Test]
        public void Watcher_Start()
        {
            // Todo: Переделать тест
            IWatcher watcher = new Components.Watcher();
            watcher.Extantion = FileExtantion.Tiff;
            watcher.Directory = new DirectoryInfo(@"\\SERVER\Scans");
            watcher.Run();

            FileInfo file = null;

            while (file == null)
            {
                file = watcher.File;
            }

            watcher.Stop();

            Assert.IsTrue(file.Exists);
        }
    }
}
